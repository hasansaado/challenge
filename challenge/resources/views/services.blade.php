<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html,
        body {
            background-color: #000;
            height: 100%;
            width: 100%;
            /* overflow: hidden; */
            overflow-y: auto;
        }

        .center-fit {
            color: #636b6f;
            /* font-family: 'Nunito', sans-serif; */
            font-weight: 200;
            height: 100%;
            width: 100%;
            /* position: relative;
            background: url("/images/services-bg.png") no-repeat;
            background-size: 100% 100%;
            
            background-position-y: center;
            background-position-x: center; */

            text-align: center;
        }

        .body {
            outline: #000 solid 50px;
            max-height: 100%;
            width: 90%;
            margin-left: auto;
            margin-right: auto;

        }

        .centered {
            /* margin-top: 100%; */
            overflow-y: auto;
            color: white;
            max-height: 83%;
            height: 83%;
            max-width: 81%;
            width: 81%;
            background: url("/images/services-bg.png") no-repeat;
            background-size: 97% 135%;
            background-position-y: center;
            background-position-x: center;
            position: absolute;
            margin-left: auto;
            margin-left: auto;
        }

        .shadow {
            background-color: rgba(0, 0, 0, 0.356);
        }



        .border {



            border-left: 1px solid #3e3e3e;
            border-right: 1px solid #3e3e3e;

        }

        .bordertop {
            border-bottom: 1px solid #3e3e3e;
            height: 50px;
            border-top: none;
            text-align: center;
        }

        .borderbottom {
            margin-top: 100px;
            text-align: center;
            border-top: 1px solid #3e3e3e;
            height: 70px;
            border-bottom: none;
        }

        .logo {
            width: 80px;
            height: auto;
            margin-top: 10px;
        }

        .row {
            display: flex;
            flex-wrap: wrap;
            position: fixed;
            height: 100%;
            width: 95%
        }

        .roww {
            display: flex;
            flex-wrap: wrap;
        }

        .roww h2 {
            margin-left: auto;
            margin-right: auto;
            font-weight: Regular;
            max-width: 300px;
            font-family: Roboto;
            max-height: 101px;
            line-height: 15px;
        }

        .text {
            margin-left: auto;
            margin-right: auto;
            font-weight: Regular;
            font-size: 12px;
            max-width: 300px;
            font-family: Roboto;
            max-height: 101px;
            line-height: 15px;
        }

        .col-md-10 {
            flex: 0 0 81.333333%;
            max-width: 81.333333%;
        }

        .col-md-4 {
            flex: 0 0 33.333333%;
            max-width: 33.333333%;
        }
        .col-md-8 {
            flex: 0 0 66.333333%;
            max-width: 50.333333%;
        }

        .col-md-1 {
            flex: 0 0 8.333333%;
            max-width: 8.333333%;

        }

        .social-media {
            margin-top: auto;
            margin-bottom: auto;
        }

        .title {
            font: 'Indivisible';
            align-content: center;
            align-items: left;
            font-weight: bold;
            margin-left: 50px;
            margin-top: 50px
        }

        .nav {
            margin-top: 80px;
            font-family: sans-serif;
            font-size: 12px;
            color: white;
            width: 150px;
            height: 10px;
            -ms-transform: rotate(-90deg);
            /* IE 9 */
            -webkit-transform: rotate(-90deg);
            /* Safari 3-8 */
            transform: rotate(-90deg);
        }

        .nav2 {
            margin-top: 170px;
            font-family: sans-serif;
            font-size: 12px;
            color: white;
            width: 150px;
            height: 10px;
            -ms-transform: rotate(-90deg);
            /* IE 9 */
            -webkit-transform: rotate(-90deg);
            /* Safari 3-8 */
            transform: rotate(-90deg);
        }

        .nav3 {
            margin-top: 70px;
            font-family: sans-serif;
            font-size: 12px;
            color: white;
            width: 150px;
            height: 10px;
            -ms-transform: rotate(-90deg);
            /* IE 9 */
            -webkit-transform: rotate(-90deg);
            /* Safari 3-8 */
            transform: rotate(-90deg);
        }
        .link{
            border-right: 6px solid blue;
            width: 15px;
            height: 53px;
            margin-left: 86px;
            margin-top: -33px;
        }

    </style>
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"> -->
</head>

<body>

    <div class="center-fit">

        <div class="body">

            <div class="row">

            <div class="col-md-1">
                    <a href="/" style="text-decoration: none">
                        <div class="nav" style="border-left: 1px  #FFF">
                            Home
                           
                        </div>
                      
                    </a>
                    <a href="services" style="text-decoration: none">
                        <div class="nav">
                            Services
                        </div>
                    </a>
                    <div class="link"></div>
                    <a href="#" style="text-decoration: none">
                        <div class="nav">
                            Portfolio
                        </div>
                    </a>
                    <a href="#" style="text-decoration: none">
                        <div class="nav2">
                            Blog
                        </div>
                    </a>
                    <a href="#" style="text-decoration: none">
                        <div class="nav3">
                            Contact us
                        </div>
                    </a>
                </div>

                <div class="border col-md-10">
                    <div class="bordertop">

                        <img class="logo" src="{{ URL::to('/images/logo.png') }}">
                    </div>


                    <div class="centered">
                        <div>
                            <div class="roww">
                                <div class="col-md-1">
                                    <div class="title" style="font-family: Indivisible;font-weight: Black;font-size:47px">
                                        SERVICES
                                    </div>
                                </div>
                            </div>

                            <div class="roww" style="margin-top: 70px">
                                <div class='col-md-4'>

                                </div>
                                <div class='col-md-8'  style="margin-left: 0px;text-align: left">
                                
                                <div class="">
                                <div>Our Mission</div>
                                   <p> E-mood was launched in 2018 by CEO Bernadette Saliba. We at e-mood believe in the future, we believe that good social media is the backbone of every business looking to prosper in 2019 and we strive to provide the utmost quality skills and strategies to get your business to where it should be. Whether its e-commerce, social media management and content creation or brand conceptualization, our team of social media and strategy experts are always looking for new and improved way to further expand your businesses’ reach and ROI.</p>

                                    Why are we doing this you ask? Our goal is to become the leading experts in the field of e-commerce and social media and use our experience and expertise to boost your brand to new heights.

                                </div>
                                </div>
                                
                                
                            </div>

                        </div>
                    </div>
                    <div style="height: 68%;width: 50%"></div>
                    <div class="borderbottom">
                        <p style="font-family:roboto;font-weight: Black;font-size:10px;">
                            © TheWebAddicts.com 2019
                        </p>
                    </div>
                </div>
                <div class="col-md-1 social-media">

                    <img class="" src="{{ URL::to('/images/social-media.png') }}" />

                </div>

            </div>

        </div>

    </div>

</body>

</html>