<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet"> -->

    <!-- Styles -->
    <style>
        html,
        body {
            background-color: #000;
            height: 100%;
            width: 100%;
            overflow: hidden;
        }

        .center-fit {
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100%;
            width: 100%;
            position: relative;
            background: url("/images/MOSHED-2019-11-29-12-42-34.png") no-repeat;
            background-size: 97% 135%;
            background-position-y: -200px;
            background-position-x: center;
            text-align: center;
        }

        .body {
            outline: #000 solid 50px;
            height: 100%;
            width: 90%;
            margin-left: auto;
            margin-right: auto;

        }

        .centered {
            margin-top: 300px;
            left: 50%;
            color: white;
        }

        .shadow {
            background-color: rgba(0, 0, 0, 0.356);
        }

        .text {
            margin-left: auto;
            margin-right: auto;
            font-weight: Regular;
            font-size: 12px;
            max-width: 300px;
            font-family: Roboto;
            max-height: 101px;
            line-height: 15px;
        }

        .border {

            /* height: 100%; */


            border-left: 1px solid #3e3e3e;
            border-right: 1px solid #3e3e3e;
        }

        .bordertop {
            border-bottom: 1px solid #3e3e3e;
            height: 50px;
            border-top: none;
            text-align: center;
        }

        .borderbottom {
            margin-top: 100px;
            text-align: center;
            border-top: 1px solid #3e3e3e;
            height: 70px;
            border-bottom: none;
        }

        .logo {
            width: 80px;
            height: auto;
            margin-top: 10px;
        }

        .row {
            display: flex;
            flex-wrap: wrap;
        }

        .col-md-10 {
            flex: 0 0 81.333333%;
            max-width: 81.333333%;
        }

        .col-md-1 {
            flex: 0 0 8.333333%;
            max-width: 8.333333%;

        }

        .social-media {
            margin-top: auto;
            margin-bottom: auto;
        }

        .nav {
            margin-top: 80px;
            font-family: sans-serif;
            font-size: 12px;
            color: white;
            width: 150px;
            height: 10px;
            -ms-transform: rotate(-90deg);
            /* IE 9 */
            -webkit-transform: rotate(-90deg);
            /* Safari 3-8 */
            transform: rotate(-90deg);
        }

        .nav2 {
            margin-top: 170px;
            font-family: sans-serif;
            font-size: 12px;
            color: white;
            width: 150px;
            height: 10px;
            -ms-transform: rotate(-90deg);
            /* IE 9 */
            -webkit-transform: rotate(-90deg);
            /* Safari 3-8 */
            transform: rotate(-90deg);
        }

        .nav3 {
            margin-top: 70px;
            font-family: sans-serif;
            font-size: 12px;
            color: white;
            width: 150px;
            height: 10px;
            -ms-transform: rotate(-90deg);
            /* IE 9 */
            -webkit-transform: rotate(-90deg);
            /* Safari 3-8 */
            transform: rotate(-90deg);
        }

        .colorstrip {
            top: 0;
            left: 0;
            width: 200px;
            height: 100%;
            color: #3e3e3e;
        }
        .link{
            border-right: 6px solid blue;
            width: 15px;
            height: 50px;
            margin-left: 83px;
            margin-top: -27px;
        }
    </style>
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"> -->
</head>

<body>

    <div class=" center-fit ">

        <div class="body">
            <div class="row">
                <div class="col-md-1">
                    <a href="/" style="text-decoration: none">
                        <div class="nav">
                            Home
                        </div>
                    </a>
                    <div class="link"></div>
                    <a href="services" style="text-decoration: none">
                        <div class="nav">
                            Services
                        </div>
                    </a>
                    <a href="#" style="text-decoration: none">
                        <div class="nav">
                            Portfolio
                        </div>
                    </a>
                    <a href="#" style="text-decoration: none">
                        <div class="nav2">
                            Blog
                        </div>
                    </a>
                    <a href="#" style="text-decoration: none">
                        <div class="nav3">
                            Contact us
                        </div>
                    </a>
                </div>

                <div class="border  col-md-10">
                    <div class="bordertop">
                        <img class="logo" src="{{ URL::to('/images/logo.png') }}">
                    </div>

                    <div class="centered ">
                        <div>
                            <h3 style="font-family:roboto;font-weight: Black;font-size:25px;">
                                TURNING BRANDS INTO LEGENDS
                            </h3>
                        </div>

                        <div class="text">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            Ut enim ad minim veniam, exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </div>
                    </div>
                    <div class="borderbottom">
                        <p style="font-family:roboto;font-weight: Black;font-size:10px;">
                            © TheWebAddicts.com 2019
                        </p>
                    </div>
                </div>

                <div class="col-md-1 social-media">

                    <img class="" src="{{ URL::to('/images/social-media.png') }}" />

                </div>
            </div>


        </div>
    </div>

</body>

</html>